package com.example.demo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.example.demo.entities.MainBookCase;
import com.example.demo.entities.MainBookCaseDao;
import com.example.demo.services.MainBookCaseService;

import javax.xml.ws.Endpoint;




/**
 *
 */
@ApplicationPath("/data")
public class DemoRestApplication extends Application {
	
	@Inject
	private GlAcountDao glAcountDao;
	
	@Inject
	private MainBookCaseService mainBookCaseService;
	
	@Inject
	private GlAccountService glAccountService;
	
	
	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) throws MalformedURLException {
		Endpoint.publish("http://localhost:8182/service/bottomup", new BottomUpServiceImpl(glAccountService));
		/*
		 * if (glAcountDao == null) { System.out.println("XXXXXXXXXXXXXXXXX"); }
		 * 
		 * bottomUpServiceImpl.setGlAcountDao(glAcountDao);
		 */
		
		List<GlAccount> all = glAcountDao.readAll();
		
		List<MainBookCase> allCases = mainBookCaseService.readAll();
		
		/*
		URL url = new URL("http://localhost:8181/service/bottomup?wsdl");
		
        //1st argument service URI, refer to wsdl document above
		//2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://demo.example.com/", "BottomUpServiceImpl");

        Service service = Service.create(url, qname);

        BottomUpServiceInterface hello = service.getPort(BottomUpServiceInterface.class);
*/
        System.out.println("Pocet " + all.size());
		System.out.println("Pocet " + allCases.size());
		System.out.println("Pocet rows u 1st " + allCases.get(0).getMainBookRows().size());
		
		
	}
	
}
