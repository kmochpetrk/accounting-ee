package com.example.demo;

public enum GlAccountType {
    BD,  //balance debit
    BC,  //balance credit
    PLD, //profit and loss debit
    PLC  //profit and loss credit
}
