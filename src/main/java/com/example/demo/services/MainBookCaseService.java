package com.example.demo.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.example.demo.entities.MainBookCase;
import com.example.demo.entities.MainBookCaseDao;

@ApplicationScoped
public class MainBookCaseService {
	
	@Inject
	private MainBookCaseDao mainBookCaseDao;
	
	public List<MainBookCase> readAll() {
		return mainBookCaseDao.readAll();
	}

}
