package com.example.demo;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class GlAccountService {
	
	@Inject
	private GlAcountDao glAcountDao;
	
	@Transactional
	public List<GlAccount> readAll() {
		return glAcountDao.readAll();
	}
	
    @Transactional
    public Response createNew(GlAccount glAccount) {
        return glAcountDao.createNew(glAccount);
    }

}
