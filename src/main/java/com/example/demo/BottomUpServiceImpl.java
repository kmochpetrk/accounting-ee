package com.example.demo;
import javax.ejb.Stateless;


import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.core.Response;

@WebService(endpointInterface = "com.example.demo.BottomUpServiceInterface")
public class BottomUpServiceImpl implements BottomUpServiceInterface {
	
	
    public BottomUpServiceImpl(GlAccountService glAccountService) {
		super();
		this.glAccountService = glAccountService;
	}



	private GlAccountService glAccountService;

	@Override
	@WebMethod
	public List<GlAccount> getAllAccounts() {
		
		return glAccountService.readAll();
	}

	@Override
	@WebMethod
	public GlAccount getAccountByAcctno(String acctno) {
		List<GlAccount> collect = glAccountService.readAll().stream().filter(one -> one.getAcctNo().equals(acctno)).collect(Collectors.toList());
		return collect.get(0);
	}

	@Override
	@WebMethod
	public String getPokus() {
		// TODO Auto-generated method stub
		return glAccountService == null ? "pokus" : "nepokus";
	}

	@Override
	@WebMethod
	public Response createNew(GlAccount glAccount) {
		// TODO Auto-generated method stub
		return glAccountService.createNew(glAccount);
	}

}
