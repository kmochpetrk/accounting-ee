package com.example.demo.entities;

public enum MbWriteType {
    DEBIT,
    CREDIT
}
