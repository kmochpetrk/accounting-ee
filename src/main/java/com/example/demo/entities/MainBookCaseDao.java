package com.example.demo.entities;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class MainBookCaseDao {
	
    @PersistenceContext(name = "prod1")
    private EntityManager em;
	
	public List<MainBookCase> readAll() {
        List resultList = em.createQuery("select m from MainBookCase m").getResultList();
        System.out.println("Main book cases number " + resultList.size());
        return resultList;
	}

}
