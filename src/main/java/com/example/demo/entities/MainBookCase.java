package com.example.demo.entities;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "main_book_case")
public class MainBookCase {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "DESCRIPTION")
    private String description;
	
    @Enumerated(EnumType.STRING)
    @Column(name = "MAIN_BOOK_CASE_TYPE")
    private MainBookCaseType mainBookCaseType;
    
    @OneToMany(mappedBy = "mainBookCase", cascade = CascadeType.ALL)
    private Set<MainBookRow> mainBookRows;
    

	public Set<MainBookRow> getMainBookRows() {
		return mainBookRows;
	}

	public void setMainBookRows(Set<MainBookRow> mainBookRows) {
		this.mainBookRows = mainBookRows;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MainBookCaseType getMainBookCaseType() {
		return mainBookCaseType;
	}

	public void setMainBookCaseType(MainBookCaseType mainBookCaseType) {
		this.mainBookCaseType = mainBookCaseType;
	}
	
}
