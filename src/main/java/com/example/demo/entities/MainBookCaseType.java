package com.example.demo.entities;

public enum MainBookCaseType {
	BANK_TRANSACTIONS_LIST,
	OUTPUT_INVOICE,
	INPUT_INVOICE,
	CASH_RECEIPT_INCOME,
	CASH_RECEIPT_OUTCOME

}
