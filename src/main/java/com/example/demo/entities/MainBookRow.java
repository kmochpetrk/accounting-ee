package com.example.demo.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.GlAccount;

@Entity
@Table(name="main_book_row")
public class MainBookRow {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "MB_WRITE_TYPE")
    private MbWriteType mbWriteType;
    
    @ManyToOne
    @JoinColumn(name = "gl_account")
    private GlAccount glAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_book_case")
    private MainBookCase mainBookCase;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public MbWriteType getMbWriteType() {
		return mbWriteType;
	}

	public void setMbWriteType(MbWriteType mbWriteType) {
		this.mbWriteType = mbWriteType;
	}

	public GlAccount getGlAccount() {
		return glAccount;
	}

	public void setGlAccount(GlAccount glAccount) {
		this.glAccount = glAccount;
	}

	public MainBookCase getMainBookCase() {
		return mainBookCase;
	}

	public void setMainBookCase(MainBookCase mainBookCase) {
		this.mainBookCase = mainBookCase;
	}
	
	

}
