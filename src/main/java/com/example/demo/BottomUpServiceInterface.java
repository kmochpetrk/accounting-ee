package com.example.demo;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.core.Response;

@WebService
public interface BottomUpServiceInterface {
	
	@WebMethod
	public List<GlAccount> getAllAccounts();
	
	@WebMethod
	public GlAccount getAccountByAcctno(String acctno);
	
	@WebMethod
	public String getPokus();
	
	@WebMethod
	public Response createNew(GlAccount glAccount);

}
